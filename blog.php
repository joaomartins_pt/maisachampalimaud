<?php include 'includes/head.php' ?>
<body>
	<?php include 'includes/nav.php' ?>
	<main id="main">
		<section class="container news-container">
			<div class="row">
				<div class="col-xs-12 col-sm-12 col-md-3">
					<h2 class="section-title">blog</h2>
				</div>
				<div class="col-xs-12 col-sm-12 col-md-9 news-grid grid">
					<a href="news-page.php" class="news-list-item grid-item">
						<div class="img-container">
						<img class="" src="img/f0235e_aff525505e5e4494b0d3f2518bdeea40.jpg" alt="">
					</div>
						<span class="news-list-date">20.09.2018</span>
						<span class="news-list-title"><span class="">Title</span> </span>
					</a>
					<a href="news-page.php" class="news-list-item grid-item">
						<div class="img-container">
						<img class="" src="img/f0235e_0e607e42acaa403e8219fa262da904ca.jpg" alt="">
					</div>
						<span class="news-list-date">20.09.2018</span>
						<span class="news-list-title"><span class="">Title</span> </span>
					</a>
						<a href="news-page.php" class="news-list-item grid-item">
						<div class="img-container">
						<img class="" src="img/f0235e_aff525505e5e4494b0d3f2518bdeea40.jpg" alt="">
					</div>
						<span class="news-list-date">20.09.2018</span>
						<span class="news-list-title"><span class="">Title</span> </span>
					</a>
					<a href="news-page.php" class="news-list-item grid-item">
						<div class="img-container">
						<img class="" src="img/f0235e_8323089bd5bf4760b82302d4534a162d.jpg" alt="">
					</div>
						<span class="news-list-date">20.09.2018</span>
						<span class="news-list-title"><span class="">Title</span> </span>
					</a>
				</div>
			</div>

		</section>
	</main>
	<?php include 'includes/footer.php' ?>

	<script src="dist/js/bundle.js"></script>
</body>
</html>
