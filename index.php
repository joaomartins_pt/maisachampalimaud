<?php include 'includes/head.php' ?>
<body>
	<?php include 'includes/nav.php' ?>
	<main id="main">
		<h1 class="sr-only">Maísa Champalimaud Portfolio</h1>
		<div class="swiper-container swiper-hero">
			<div class="swiper-wrapper">
				<div class="swiper-slide">
					<div class="swiper-hero-img" style="background-image:url(img/f0235e_0e607e42acaa403e8219fa262da904ca.jpg)">
					</div>
					<div class="swiper-hero-info" style="background-color: #FFF0DE">
						<span class="swiper-hero-title">As cores do Mundo</span>
						<a href="project-page.php" class="swiper-hero-link underline">view more</a>
					</div>
				</div>
				<div class="swiper-slide">
					<div class="swiper-hero-img" style="background-image:url(img/f0235e_db3d0f83cd7542ed96ce2600d971b8e7.jpg)">
					</div>
					<div class="swiper-hero-info" style="background-color: #c9f0ff">
						<span class="swiper-hero-title">royal blue 227 #3</span>
						<a href="project-page.php" class="swiper-hero-link underline">view more</a>
					</div>
				</div>
			</div>
			<div class="swiper-pagination"></div>
			<div class="swiper-button-prev"><img src="dist/assets/nav-left.svg"></div>
			<div class="swiper-button-next"><img src="dist/assets/nav-right.svg"></div>
		</div>
		<section class="container">
			<div class="row">
				<div class="col-xs-12 col-sm-12 col-md-3">
					<h2 class="section-title">recent projects</h2>
				</div>
				<div class="col-xs-6 col-sm-4 col-md-3 pad-right">
					<a href="project-page.php" class="project-list-item">
						<div class="img-container">
							<img class="" src="img/f0235e_0e607e42acaa403e8219fa262da904ca.jpg" alt="">
						</div>
						<span class="project-list-title ">Title </span>
						<span class="project-list-sub-title">Subtitle subtitle subtitle</span>
					</a>
				</div>
				<div class="col-xs-6 col-sm-4 col-md-3 pad-left">
					<a href="project-page.php" class="project-list-item">
						<div class="img-container">
							<img class="" src="img/f0235e_aff525505e5e4494b0d3f2518bdeea40.jpg" alt="">
						</div>
						<span class="project-list-title ">Title title </span>
						<span class="project-list-sub-title">Subtitle subtitle subtitle</span>
					</a>
				</div>
				<div class="col-xs-6 col-sm-4 col-md-3 hidden-xs">
					<a href="project-page.php" class="project-list-item">
						<div class="img-container">
							<img class="" src="img/f0235e_8323089bd5bf4760b82302d4534a162d.jpg" alt="">
						</div>
						<span class="project-list-title ">Title title</span>
						<span class="project-list-sub-title">Subtitle subtitle subtitle</span>
					</a>
				</div>
				<div class="col-md-3 col-md-offset-6 col-xs-12">
					<a class="box-link" href="projects.php"> view more </a>
				</div>
			</div>

		</section>
		<section class="container">
			<div class="row">
				<div class="col-xs-12 col-sm-12 col-md-3">
					<h2 class="section-title">recent news</h2>
				</div>
				<div class="col-xs-6 col-sm-4 col-md-3 pad-right">
					<a href="news-page.php" class="news-list-item">
						<div class="img-container">
							<img class="" src="img/f0235e_0e607e42acaa403e8219fa262da904ca.jpg" alt="">
						</div>
						<span class="news-list-date">20.09.2018</span>
						<span class="news-list-title"><span class="">Title</span></span>
					</a>
				</div>
				<div class="col-xs-6 col-sm-4 col-md-3 pad-left">
					<a href="news-page.php" class="news-list-item">
						<div class="img-container">
							<img class="" src="img/f0235e_8323089bd5bf4760b82302d4534a162d.jpg" alt="">
						</div>
						<span class="news-list-date">20.09.2018</span>
						<span class="news-list-title"><span class="">Title</span> </span>
					</a>
				</div>
				<div class="col-xs-6 col-sm-4 col-md-3 hidden-xs">
					<a href="news-page.php" class="news-list-item">
						<div class="img-container">
							<img class="" src="img/f0235e_aff525505e5e4494b0d3f2518bdeea40.jpg" alt="">
						</div>
						<span class="news-list-date">20.09.2018</span>
						<span class="news-list-title"><span class="">Title</span> </span>
					</a>
				</div>
				<div class="col-md-3 col-md-offset-6 col-xs-12">
					<a class="box-link" href="blog.php"> view more </a>
				</div>
			</div>

		</section>
	</main>
	<?php include 'includes/footer.php' ?>

	<script src="dist/js/bundle.js"></script>
</body>
</html>
