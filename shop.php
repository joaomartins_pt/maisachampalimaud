<?php include 'includes/head.php' ?>
<body>
	<?php include 'includes/nav.php' ?>
	<main id="main">
		<section class="container projects-container">
			<div class="row">
				<div class="col-xs-12 col-sm-12 col-md-3">
					<h2 class="section-title">shop</h2>
					<div class="text-container">
						<p class="basic-paragraph">
							Todas as compras e encomendas são feitas por e-mail.
						</p>
						<p class="basic-paragraph">
	Todas as obras são reproduções, a loja online não se destina a encomendas personalizadas.
	</p>
					</div>
				</div>
				<div class="col-xs-12 col-sm-12 col-md-9 projects-grid grid">
					<a href="product-page.php" class="project-list-item grid-item">
						<div class="img-container">
							<img class="" src="img/f0235e_0e607e42acaa403e8219fa262da904ca.jpg" alt="">
						</div>
						<span class="project-list-title">Title title Title title title title</span>
						<span class="project-list-price">280€</span>
					</a>
					<a href="product-page.php" class="project-list-item grid-item">
						<div class="img-container">
							<img class="" src="img/f0235e_8323089bd5bf4760b82302d4534a162d.jpg" alt="">
						</div>
						<span class="project-list-title">Title title</span>
						<span class="project-list-price">300€</span>
					</a>
					<a href="product-page.php" class="project-list-item grid-item">
						<div class="img-container">
							<img class="" src="img/f0235e_0e607e42acaa403e8219fa262da904ca.jpg" alt="">
						</div>
						<span class="project-list-title">Title title Title title title title</span>
						<span class="project-list-price">280€</span>
					</a>
					<a href="product-page.php" class="project-list-item grid-item">
						<div class="img-container">
							<img class="" src="img/f0235e_8323089bd5bf4760b82302d4534a162d.jpg" alt="">
						</div>
						<span class="project-list-title">Title title</span>
						<span class="project-list-price">300€</span>
					</a>
					<a href="product-page.php" class="project-list-item grid-item">
						<div class="img-container">
							<img class="" src="img/f0235e_0e607e42acaa403e8219fa262da904ca.jpg" alt="">
						</div>
						<span class="project-list-title">Title title Title title title title</span>
						<span class="project-list-price">280€</span>
					</a>
					<a href="product-page.php" class="project-list-item grid-item">
						<div class="img-container">
							<img class="" src="img/f0235e_8323089bd5bf4760b82302d4534a162d.jpg" alt="">
						</div>
						<span class="project-list-title">Title title</span>
						<span class="project-list-price">300€</span>
					</a>
				</div>
			</div>

		</section>
	</main>
	<?php include 'includes/footer.php' ?>

	<script src="dist/js/bundle.js"></script>
</body>
</html>
