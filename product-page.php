<?php include 'includes/head.php' ?>
<body>
	<?php include 'includes/nav.php' ?>
	<main id="main">

		<section class="product-section">
			<div class="container">
				<div class="row">
					<div class="col-md-6 col-sm-12">
						<h2 class="product-title">Série “Entre a distância e o amanhecer”</h2>
						<span class="product-price">300€</span>
							<p class="basic-paragraph">
								Formato: 100 x 70 cm<br>
Materiais: Acrilico sobre papel 200g Cópias: 30<br>
(Moldura não incluída)
							</p>
							<a class="box-link expand-order-toggle">encomendar</a>
					</div>
					<div class="col-md-6 col-sm-12">
						<div class="swiper-container swiper-product swiper-slideshow">
							<div class="swiper-wrapper">
								<div class="swiper-slide">
									<img src="img/Asset 3.png">
								</div>
								<div class="swiper-slide">
									<img src="img/f0235e_8323089bd5bf4760b82302d4534a162d.jpg">
								</div>
								<div class="swiper-slide">
									<img src="img/Asset 2.png">
								</div>
							</div>
							<div class="swiper-pagination"></div>
							<div class="swiper-button-prev"><img src="dist/assets/nav-left.svg"></div>
							<div class="swiper-button-next"><img src="dist/assets/nav-right.svg"></div>
						</div>
					</div>
				</div>
			</section>
			<div class="expand-order">
				<div class="close"></div>
					<div class="container">
						<div class="col-md-6 col-sm-12">
							<h2 class="product-title">Encomendar</h2>
							<span class="product-sub-title bold">Série “entre a distancia e o amanhecer”</span>
						</div>
						<div class="col-md-6 col-sm-12">
							<div class="order-sent"><span>Obrigado pelo seu pedido, entraremos em contacto brevemente</span></div>
							<form  class="order-form" action="" autocomplete="off">
								<label>Assunto</label>
							  <input class="assunto" name="e-mail" placeholder="Série “entre a distancia e o amanhecer”" disabled></textarea>
							  <label>Contactos</label>
								<input  name="name" placeholder="Nome e Apelido" autocomplete="off" ></textarea>
							  <input class="half-width"  name="e-mail" placeholder="E-mail" autocomplete="off" ></textarea>
								<input class="half-width"  name="telephone" placeholder="Telefone" autocomplete="off" ></textarea>
								<label>Tem alguma questão?</label>
								<textarea class="input-message" name="message" placeholder="A sua mensagem" autocomplete="off" ></textarea>
							  <a class="box-link send-order">enviar pedido</a>
							</form>
						</div>
				</div>
			</div>
		</main>
		<div class="hidden-xs"><?php include 'includes/footer.php' ?></div>

		<script src="dist/js/bundle.js"></script>
	</body>
	</html>
