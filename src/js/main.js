global.$ = require('jquery');
global.jQuery = require('jquery');

const ObjectFitImages = require('object-fit-images');
const Swiper = require('swiper');
const Isotope = require('isotope-layout');

var mobile_w = 767;
var mobile_down = window.matchMedia( "(max-width: 767px)" )

ObjectFitImages();


(function () {
	$('.menu-wrapper').on('click', function() {
		$('.hamburger-menu, .site-header').toggleClass('active');
	})
})();

var heroSwiper = new Swiper('.swiper-hero', {
	speed: 400,
	spaceBetween: 0,
	navigation: {
		nextEl: '.swiper-hero .swiper-button-next',
		prevEl: '.swiper-hero .swiper-button-prev'
	},
});
var slideshowSwiper = new Swiper('.swiper-slideshow', {
	speed: 600,
	spaceBetween: 0,
	simulateTouch: false,
	effect: "fade",
	fadeEffect: {
		crossFade: true
	},
	loop: true,
	navigation: {
		nextEl: '.swiper-button-next',
		prevEl: '.swiper-button-prev'
	},
	pagination: {
    el: '.swiper-pagination',
    type: 'bullets',
  },
	on: {
		init: function () {
			var slides_number = $('.swiper-slide').length;
			var slides_duplicate_number = $('.swiper-slide-duplicate').length;
			var real_slides_number = slides_number - slides_duplicate_number;
			if (real_slides_number == 1){
				$(".swiper-button-prev, .swiper-button-next").hide();
				$(".swiper-pagination").css("opacity", 0);
			}
		},
	},
});
var productSwiper = new Swiper('.swiper-product', {
	speed: 600,
	spaceBetween: 0,
	simulateTouch: false,
	effect: "fade",
	fadeEffect: {
		crossFade: true
	},
	loop: true,
	navigation: {
		nextEl: '.swiper-button-next',
		prevEl: '.swiper-button-prev'
	},
	pagination: {
    el: '.swiper-pagination',
    type: 'fraction',
  },
});


$(document).ready(function(){

	$(".slide-toggle").click(function(){
		if(mobile_down.matches){
			$(this).parent().siblings().find(".collapse").slideToggle(300);
		}
	});
	$(".more-info-toggle").click(function(){
		$(".expand-info").slideToggle(300);
		$(this).toggleClass("active");
	});
	$(".expand-order-toggle").click(function(){
		$(".expand-order").slideToggle(300, "swing");
		$(".product-section").toggleClass("transparent");
	});
	$(".close").click(function(){
		$(".expand-order").slideToggle(300);
				$(".product-section").removeClass("transparent");
	});
	$(".send-order").click(function(){
		$(".order-sent").slideDown(200);
	});
	$(".arrow-up").click(function(){
		$('html, body').animate({
			scrollTop: 0,
		}, 500);
	})
	if ($(".grid").length){
		var iso = new Isotope( '.grid', {
			itemSelector: '.grid-item',
			layoutMode: 'fitRows',
			filter: '.grid-item',
			hiddenStyle: {
			opacity: 0,
    },
    visibleStyle: {
			opacity: 1,
    }
		});
	}
	$('.primary-nav a').each(function(){
            if ($(this).prop('href') == window.location.href) {
                $(this).addClass('active');
                $(this).parent('li').addClass('active');
            }
        });
	var data_filter;
	$(".filter-button").click(function() {
		data_filter = $(this).data("filter");
		$(this).siblings().removeClass("is-checked");
		$(this).toggleClass("is-checked");
		if($(".is-checked").length){
			iso.arrange({
  			filter: '.' + data_filter,
			})

		} else {
			iso.arrange({
  			filter: '.grid-item',
			})
		}
	});

});
