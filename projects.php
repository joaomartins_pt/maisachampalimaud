<?php include 'includes/head.php' ?>
<body>
	<?php include 'includes/nav.php' ?>
	<main id="main">
		<section class="container projects-container">
			<div class="row">
				<div class="col-xs-12 col-sm-12 col-md-3">
					<h2 class="section-title">projects</h2>
					<div class="filters-container">
						<button class="filter-button" data-filter="mural">mural</button>
						<button class="filter-button" data-filter="ink-sketches">ink sketches</button>
						<button class="filter-button"  data-filter="photography">photography</button>
						<button class="filter-button"  data-filter="painting">painting</button>
					</div>
				</div>
				<div class="col-xs-12 col-sm-12 col-md-9 projects-grid grid">
					<a href="project-page.php" class="project-list-item grid-item ink-sketches">
						<div class="img-container">
							<img class="" src="img/f0235e_0e607e42acaa403e8219fa262da904ca.jpg" alt="">
						</div>
						<span class="project-list-title ">Title title Title title title title Title title Title title title title</span>
						<span class="project-list-sub-title">Subtitle subtitle subtitle</span>
					</a>
					<a href="project-page.php" class="project-list-item grid-item mural">
						<div class="img-container">
							<img class="" src="img/f0235e_8323089bd5bf4760b82302d4534a162d.jpg" alt="">
						</div>
						<span class="project-list-title ">Title title</span>
						<span class="project-list-sub-title">Subtitle subtitle subtitle</span>
					</a>
					<a href="project-page.php" class="project-list-item grid-item photography">
						<div class="img-container">
							<img class="" src="img/f0235e_0e607e42acaa403e8219fa262da904ca.jpg" alt="">
						</div>
						<span class="project-list-title ">Title title</span>
						<span class="project-list-sub-title">Subtitle subtitle subtitle</span>
					</a>
					<a href="project-page.php" class="project-list-item grid-item painting">
						<div class="img-container">
							<img class="" src="img/f0235e_aff525505e5e4494b0d3f2518bdeea40.jpg" alt="">
						</div>
						<span class="project-list-title ">Title title title</span>
						<span class="project-list-sub-title">Subtitle subtitle subtitle</span>
					</a>
					<a href="project-page.php" class="project-list-item grid-item photography">
						<div class="img-container">
							<img class="" src="img/f0235e_0e607e42acaa403e8219fa262da904ca.jpg" alt="">
						</div>
						<span class="project-list-title ">Title title Title title title title</span>
						<span class="project-list-sub-title">Subtitle subtitle subtitle</span>
					</a>
								<a href="project-page.php" class="project-list-item grid-item ink-sketches">
						<div class="img-container">
							<img class="" src="img/f0235e_8323089bd5bf4760b82302d4534a162d.jpg" alt="">
						</div>
						<span class="project-list-title ">Title title</span>
						<span class="project-list-sub-title">Subtitle subtitle subtitle</span>
					</a>
					<a href="project-page.php" class="project-list-item grid-item mural">
						<div class="img-container">
							<img class="" src="img/f0235e_0e607e42acaa403e8219fa262da904ca.jpg" alt="">
						</div>
						<span class="project-list-title ">Title title title title </span>
						<span class="project-list-sub-title">Subtitle subtitle subtitle</span>
					</a>
					<a href="project-page.php" class="project-list-item grid-item photography">
						<div class="img-container">
							<img class="" src="img/f0235e_aff525505e5e4494b0d3f2518bdeea40.jpg" alt="">
						</div>
						<span class="project-list-title ">Title title</span>
						<span class="project-list-sub-title">Subtitle subtitle subtitle</span>
					</a>
					<a href="project-page.php" class="project-list-item grid-item painting">
						<div class="img-container">
							<img class="" src="img/f0235e_8323089bd5bf4760b82302d4534a162d.jpg" alt="">
						</div>
						<span class="project-list-title ">Title title title title </span>
						<span class="project-list-sub-title">Subtitle subtitle subtitle</span>
					</a>
					<a href="project-page.php" class="project-list-item grid-item photography">
						<div class="img-container">
							<img class="" src="img/f0235e_aff525505e5e4494b0d3f2518bdeea40.jpg" alt="">
						</div>
						<span class="project-list-title ">Title title title</span>
						<span class="project-list-sub-title">Subtitle subtitle subtitle</span>
					</a>
				</div>
			</div>

		</section>
	</main>
	<?php include 'includes/footer.php' ?>

	<script src="dist/js/bundle.js"></script>
</body>
</html>
