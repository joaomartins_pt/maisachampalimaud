<?php include 'includes/head.php' ?>
<body>
	<?php include 'includes/nav.php' ?>
	<main id="main">
		<section class="container press-section">
			<div class="row">
				<div class="col-xs-12 col-sm-12 col-md-3">
					<h2 class="section-title">press</h2>
				</div>
				<div class="col-xs-12 col-sm-12 col-md-9">
					<table>
						<tr>
							<th class="date-col hidden-xs">Date</th>
							<th class="title-col hidden-xs">Title</th>
							<th class="doc-col hidden-xs">Doc</th>
						</tr>
						<tr>
							<td class="date-col">
								<span class="visible-xs bold">Date</span>
								30.09.2014
							</td>
							<td class="title-col">
								<span class="visible-xs bold">Title</span>
								<span>“Em Lusofonia” de Maísa Champalimaud ...</span>
								<a href="#" class="press-link hidden-xs underline thin">Link to the article</a>
							</td>
							<td class="press-link-mobile visible-xs">
								<span class="bold">Link</span>
								<a href="#"><img src="dist/assets/nav-right.svg"></a>
							</td>
							<td class="doc-col">
								<span class="visible-xs bold">Doc</span>

								<a href="#">
									<img src="dist/assets/nav-right.svg">
								</a>
							</td>
						</tr>
							<tr>
							<td class="date-col">
								<span class="visible-xs bold">Date</span>
								30.09.2014
							</td>
							<td class="title-col">
								<span class="visible-xs bold">Title</span>
								<span>“Em Lusofonia” de Maísa Champalimaud ...</span>
								<a href="#" class="press-link hidden-xs underline thin">Link to the article</a>
							</td>
							<td class="press-link-mobile visible-xs">
								<span class="bold">Link</span>
								<a href="#"><img src="dist/assets/nav-right.svg"></a>
							</td>
							<td class="doc-col">
								<span class="visible-xs bold">Doc</span>

								<a href="#">
									<img src="dist/assets/nav-right.svg">
								</a>
							</td>
						</tr>
							<tr>
							<td class="date-col">
								<span class="visible-xs bold">Date</span>
								30.09.2014
							</td>
							<td class="title-col">
								<span class="visible-xs bold">Title</span>
								<span>“Em Lusofonia” de Maísa Champalimaud ...</span>
								<a href="#" class="press-link hidden-xs underline thin">Link to the article</a>
							</td>
							<td class="press-link-mobile visible-xs">
								<span class="bold">Link</span>
								<a href="#"><img src="dist/assets/nav-right.svg"></a>
							</td>
							<td class="doc-col">
								<span class="visible-xs bold">Doc</span>

								<a href="#">
									<img src="dist/assets/nav-right.svg">
								</a>
							</td>
						</tr>
							<tr>
							<td class="date-col">
								<span class="visible-xs bold">Date</span>
								30.09.2014
							</td>
							<td class="title-col">
								<span class="visible-xs bold">Title</span>
								<span>“Em Lusofonia” de Maísa Champalimaud ...</span>
								<a href="#" class="press-link hidden-xs underline thin">Link to the article</a>
							</td>
							<td class="press-link-mobile visible-xs">
								<span class="bold">Link</span>
								<a href="#"><img src="dist/assets/nav-right.svg"></a>
							</td>
							<td class="doc-col">
								<span class="visible-xs bold">Doc</span>

								<a href="#">
									<img src="dist/assets/nav-right.svg">
								</a>
							</td>
						</tr>
					</table>
				</div>
			</div>
		</section>

	</main>
	<?php include 'includes/footer.php' ?>

	<script src="dist/js/bundle.js"></script>
</body>
</html>
