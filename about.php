<?php include 'includes/head.php' ?>
<body>
	<?php include 'includes/nav.php' ?>
	<main id="main">
		<section class="container biography-section">
			<div class="row">
				<div class="col-xs-12 col-sm-4 col-md-3">
					<h2 class="section-title">biography</h2>
					<img src="img/Asset1.png" class="about-img">
				</div>
				<div class="col-xs-12 col-sm-8 col-md-9 biography-description">
					<p class="basic-paragraph">
						Maisa Champalimaud (1987) is born, lives and works in Lisbon.
						She tries to achieve, through the paint capabilities, tread a path of self-knowledge.
					</p>
					<p class="basic-paragraph">
						Behind her lines, there is no type of power or ideology, but numerous influences that marked her decisively: either in the form, force, vibration, color and theme of their works.
					</p>
					<p class="basic-paragraph">
						Parallel to the painting, she participates in the management of a family office.
						She believes these both two different worlds, are the basis of her balance.
					</p>
				</div>
			</div>
		</section>
		<section class="container biography-section">
			<div class="row">
				<div class="col-xs-12 col-sm-4 col-md-3">
					<h2 class="section-title slide-toggle">private collections</h2>
				</div>
				<div class="col-xs-12 col-sm-8 col-md-9">
					<div class="collapse">
						<ul class="dotted-list">
							<li>Partac, SGPS S.A</li>
							<li> Partac, SGPS S.A, Lisbon</li>
							<li> CCA-Advogados, Lisbon</li>
							<li> AMI Foundation</li>
							<li> International Design Hotel, Lisbon</li>
							<li> Rebelo De Sousa & Associados-sociedade De Advogados, Lisbon</li>
							<li> The House – Boutique/Hotel, Lisbon</li>
							<li> Manuela António – Lawyers and Notaries, Macau​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​</li>
							<li> Galeria Opus 14, Lisbon</li>
							<li> Restaurant El Clandestino, Lisbon</li>
							<li> Building Ex-Libris, Lisbon</li>
							<li> Boutique dos Relógios, Lisbon</li>
							<li> Edge-Group - Espaço Amoreiras, Lisbon</li>
							<li> Abreu Advogados, Lisbon</li>
							<li> Private Lawyers, Lisbon</li>
							<li> Lx Factory, Lisbon</li>
							<li> PT Foundation, Lisbon</li>
						</ul>
					</div>
				</div>
			</section>
			<section class="container biography-section">
				<div class="row">
					<div class="col-xs-12 col-sm-4 col-md-3">
						<h2 class="section-title slide-toggle">collective exhibitions</h2>
					</div>
					<div class="col-xs-12 col-sm-8 col-md-9">
						<div class="collapse">
							<span class="bold">2011</span>
							<ul class="dotted-list">
								<li> XXIV Spring Salon - the Estoril Casino Art Gallery, Cascais</li>
							</ul>
							<span class="bold">2012</span>
							<ul class="dotted-list">
								<li>Finalists Painting Exhibition (10/11) of FBAUL - Galveias Palace Gallery, Lisbon</li>
								<li> GAB-A, Open Galleries of Fine Arts - School of Fine Arts, University of Lisbon, Lisbon</li>
								<li> Auction "Drawing Hope" Adobe for Women Association - Carpe Diem, Art and Research, Lisbon</li>
								<li> Painting Exhibition, Sculpture and Photography "A arte vai ao mercado" -  Cascais Market</li>
								<li> Opening Exhibition "Ministerium Club" - Terreiro do Paço, Lisbon</li>
							</ul>
							<span class="bold">2013</span>
							<ul class="dotted-list">
								<li> "ARTIS 13" - Art Gallery of the Casino Estoril, Cascais</li>
								<li> Photography Exhibition - Atelier Ana Azevedo, Lisbon</li>
								<li> 2nd International Art Exhibition of Estoril, Cascais</li>
								<li> Auction AASUL, Lisbon</li>
							</ul>
							<span class="bold">2015</span>
							<ul class="dotted-list">
								<li> Painting Exhibition "Obstáculos para um novo saber" Nogueira da Silva Museum, Braga</li>
								<li> Painting Exhibition "Baht" MOSTRA'15 by Patrícia Pires de Lima, Lisbon</li>
								<li> Painting Exhibition "Cidadela Art District," Cascais</li>
								<li> Mupi Project - Exhibition in public space in Lisbon (curator - Sandro Resende), Lisbon</li>
								<li> Painting Exhibition "Vaga Luz" Foundation House Museum Medeiros e Almeida, Lisbon</li>
								<li> Painting Exhibition "Juntos para Ajudar", Casa dos Sonhos, Lisbon</li>
							</ul>
							<span class="bold">2016</span>
							<ul class="dotted-list">
								<li> Painting Exhibition Work in paper, Galeria Opus 14, Lisbon</li>
								<li> Painting Exhibition  Fusca Concept Store, Braga</li>
								<li> Painting Exhibition ARTIS XIV - Festival Artis de Seia 2016 , Seia</li>
								<li> Painting Exhibition MOSTRA'16 by Patrícia Pires de Lima, Lisbon</li>
								<li> Painting Exhibition Draw me a Fado Art Exhibition , Charivari Lab, Lisbon</li>
							</ul>
							<span class="bold">2017</span>
							<ul class="dotted-list">
								<li> Painting Exhibition Paperworks IV, Galeria Belo-Galsterer, Lisbon</li>
								<li> Painting Exhibition ARTIS XV - Festival Artis de Seia 2017 , Seia</li>
								<li> Painting & Sculture Exhibition "Escalas Desejantes", Museu Nacional de História Natural e da Ciência , Lisbon</li>
								<li> Painting on lamps Exhibition "Capiti - crescer para o mundo", Museu da Eletricidade, Lisboa</li>
							</ul>
						</div>
					</div>
				</div>
			</section>
			<section class="container biography-section">
				<div class="row">
					<div class="col-xs-12 col-sm-4 col-md-3">
						<h2 class="section-title slide-toggle">individual exhibitions</h2>
					</div>
					<div class="col-xs-12 col-sm-8 col-md-9">
						<div class="collapse">
							<span class="bold">2013</span>
							<ul class="dotted-list">
								<li> Painting Exhibition, Design & Photography "Leve Leve", Atelier Quintinha 31, Lisbon</li>
							</ul>
							<span class="bold">2014</span>
							<ul class="dotted-list">
								<li> Painting Exhibition "Perdidos na terra do Nunca," AmiArte Gallery, Oporto</li>
								<li>Painting Exhibition "Love is the Answer", Atelier Quintinha 31, Lisbon</li>
								<li> Painting Exhibition "Malditos Burocratas", Poggenpohl, Puerto</li>
								<li> Painting Exhibition "Em Lusofonia" SRS Gallery, Lisbon</li>
							</ul>
							<span class="bold">2015</span>
							<ul class="dotted-list">
								<li>  Painting Exhibition “Pessoa”, Álvaro de Campos House, Tavira</li>
							</ul>
							<span class="bold">2016</span>
							<ul class="dotted-list">
								<li> Painting Exhibition "Between Phases", Espaço PT Andrade Corvo, Lisbon</li>
								<li> Painting Exhibition "Between distance and the present", Espaço Amoreiras, Lisbon</li>
								<li> Painting Exhibition "Between morning and evening", Boutique dos Relógios Plus Av. Liberdade, Lisbon</li>
							</ul>
							<span class="bold">2017</span>
							<ul class="dotted-list">
								<li> Painting Exhibition "Sobre livros", Casa dos Crivos, Braga</li>
								<li> Painting Exhibition "Devaneios oníricos", Casa Pau-Brasil, Lisbon</li>
							</ul>
						</div>
					</div>
				</div>
			</section>
		</main>
		<?php include 'includes/footer.php' ?>

		<script src="dist/js/bundle.js"></script>
	</body>
	</html>
