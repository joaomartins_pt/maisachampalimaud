<?php include 'includes/head.php' ?>
<body>
	<?php include 'includes/nav.php' ?>
	<main id="main">
		<section class="container contact-section">
			<div class="row">
				<div class="col-xs-12 col-sm-12 col-md-3">
					<h2 class="section-title">contact</h2>
				</div>
				<div class="col-xs-12 col-sm-12 col-md-9 contact-info">
					<p class="basic-paragraph">
						<span class="bold">atelier:</span>
						<a  class="underline thin" href="https://www.google.pt/maps/place/Rua+da+Arr%C3%A1bida+66A,+1250-034+Lisboa/@38.7195131,-9.1625445,17z/data=!3m1!4b1!4m5!3m4!1s0xd193365b32e74a7:0x9f653a734828412b!8m2!3d38.7195131!4d-9.1603558" target="_blank">rua da arrábida, 66a, 1250-034, Lisboa</a>
					</p>
					<p class="basic-paragraph">
						<span class="bold">
							email:
						</span>
						<a  class="underline thin" href="mailto:info@maisachampalimaud.com"> info@maisachampalimaud.com</a>
						<p class="basic-paragraph">
							<span class="bold">
								telefone:
							</span>
							<a class="underline thin" href="tel:+35191640139">+351 916 401 39</a>
							<p class="basic-paragraph">
								<span class="bold">
									<a class="underline" href="#">instagram</a>
								</span>
								<p class="basic-paragraph">
									<span class="bold">
										<a class="underline" href="#">facebook</a>
									</span>
								</p>
							</div>
						</div>
					</section>

				</main>
				<?php include 'includes/footer.php' ?>

				<script src="dist/js/bundle.js"></script>
			</body>
			</html>
