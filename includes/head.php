<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title> Maísa Champalimaud Portfolio</title>
	<link rel="stylesheet" type="text/css" href="dist/css/main.min.css">
	<meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="icon" type="image/png" sizes="32x32" href="img/favicon-32x32.png">
<link rel="icon" type="image/png" sizes="16x16" href="img/favicon-16x16.png">
<meta name="msapplication-TileColor" content="#da532c">
<meta name="theme-color" content="#ffffff">
</head>
