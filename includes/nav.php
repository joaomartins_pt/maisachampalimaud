<?php
if(isset($_GET['lang'])) {
	$lang = $_GET['lang'];
} else {
	$lang = 'en';
}

?>
<header class="site-header">
	<nav class="primary-nav">
		<ul>
			<li><a href="about.php" class="underline">about</a></li>
			<li><a href="projects.php" class="underline">projects</a></li>
			<li><a href="shop.php" class="underline">shop</a></li>
			<li><a href="press.php" class="underline">press</a></li>
			<li><a href="blog.php" class="underline">blog</a></li>
			<li><a href="contact.php" class="underline">contact</a></li>
		</ul>
	</nav>
	<a href="index.php" class="site-id"><img src="dist/assets/maisa.svg" alt="Maísa"></a>
	<nav class="secondary-nav">
		<li
		<?php
		if ($lang == "en") {
				echo "class='active'";
		}
		?>> <a title href="?lang=en" class="underline">en</a></li><li class="separator">-</li><li <?php
		if ($lang == "pt") {
				echo "class='active'";
		}
		?>
		> <a title href="?lang=pt" class="underline">pt</a></li>
	</nav>
	<div class="menu-wrapper">
	  <div class="hamburger-menu"></div>
	</div>

</header>
