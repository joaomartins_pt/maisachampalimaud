<footer>
	<div class="footer-content">
		<div class="social-icons">
	<a href="#" class="social-icon" >
		<svg x="0px" y="0px" viewBox="0 0 200 200" style="enable-background:new 0 0 200 200;" xml:space="preserve">
<path class="black" d="M196.1,45c-2.9-13.9-9.7-25.2-21.6-33.3C165.4,5.4,155,2.8,144.2,2c-8.5-0.7-16.9-0.8-25.4-0.8
	c-21,0-42.1-0.5-63.2,0.8c-8.1,0.5-16,2.1-23.4,5.6c-14.8,7-24,18.8-28.1,34.5C2,50,1.5,57.9,1.4,65.9C1,92.1,0.5,118.3,2,144.4
	c0.5,8.9,2.5,17.5,6.7,25.4c7.8,14.6,20.2,23,36.2,26.4c8.7,1.8,17.6,2,26.5,2.1c9.4,0.1,18.9,0,28.3,0c9.6,0,19.2,0.1,28.8,0
	c8.7-0.1,17.4-0.3,26-2c22.2-4.6,37.2-19.7,41.7-41.8c1.8-8.7,2-17.6,2-26.4c0.1-18.8,0.1-37.6,0-56.4C198.1,62.7,198,53.8,196.1,45
	z M180.5,132.6c-0.2,7.1-0.3,14.1-2.2,21c-3.9,14.3-13.5,22.5-27.8,25.4c-5.1,1-10.4,1.3-15.6,1.4c-23.4,0.6-46.9,0.6-70.3,0
	c-6.6-0.2-13.2-0.5-19.6-2.4c-13.7-4.1-21.6-13.5-24.3-27.3c-1.9-9.7-1.6-19.5-1.8-29.3c-0.2-7.2,0-14.5,0-21.7
	c0-13.8-0.2-27.5,0.6-41.2c0.3-6.4,1.3-12.8,4-18.7c4.8-10.4,13-16.4,23.9-18.9c5.8-1.4,11.7-1.6,17.6-1.8
	c17.4-0.6,34.8-0.3,52.2-0.3c9.2,0,18.4,0.1,27.5,1c7.6,0.7,14.9,2.6,21.2,7.3c7.9,5.9,11.9,14.1,13.4,23.7
	c1.5,9.9,1.4,19.9,1.5,29.9C180.8,98,181,115.3,180.5,132.6z"/>
<path class="black" d="M99.8,49.2C72.3,49,49.4,71.6,49.2,99.1c-0.2,28.2,22,51.1,49.9,51.4c28.1,0.2,51-22.1,51.3-50
	C150.6,72.1,128.2,49.5,99.8,49.2z M99.6,132.8c-18.2-0.2-32.9-14.9-32.9-33c0.1-18.2,15-33.1,33.1-32.9c18.3,0.1,33,15,32.9,33.1
	C132.6,118.3,117.8,132.9,99.6,132.8z"/>
<path class="black" d="M152,35.4c-6.4,0.2-11.7,5.7-11.5,12.1c0.2,6.5,5.6,11.7,12,11.5c6.6-0.2,11.8-5.6,11.6-12.1
	C164,40.4,158.5,35.3,152,35.4z"/>
</svg>
</a>
<a href="#" class="social-icon">
<svg   x="0px" y="0px" viewBox="0 0 94.7 200" style="enable-background:new 0 0 94.7 200;" xml:space="preserve">
<path class="black" d="M61.2,146.2c0,14.6-0.1,29.1,0,43.6c0,2.4-0.6,3.2-3.1,3.2c-10.4-0.1-20.9-0.2-31.3,0c-2.6,0-3-0.8-3-3.1
	c0.1-28.8,0-57.7,0.1-86.5c0-3.2-0.8-4-3.9-3.8c-4.3,0.3-8.6,0-13,0.1c-2,0-2.9-0.3-2.9-2.6c0.2-8.9,0.1-17.8,0-26.8
	c0-1.9,0.6-2.3,2.3-2.3c5,0,10-0.2,14.8,0c2,0,2.5-0.7,2.5-2.5c-0.1-7.7,0-15.5,0-23.2c0-4.6,0.5-9.1,1.9-13.5
	C29.6,16.4,38.9,10.4,51,8.3c12.3-2.3,24.8-0.8,37.2-1.1c1.4,0,1.8,0.6,1.8,1.9c0,9.5-0.1,19,0,28.5c0,2-1.1,2-2.4,2
	c-5.7,0-11.5-0.1-17.3,0c-7.1,0.1-9.1,2.1-9.2,9.3c0,5.4,0.1,10.8-0.1,16.2c0,2.3,1,3.2,3,3.1c7.9-0.2,15.7,0,23.6-0.1
	c1.8,0,2.3,0.4,2.2,2.3c-0.8,8.7-1.7,17.5-2.3,26.2c-0.2,2.2-0.8,2.7-2.9,2.6c-6.7,0-13.3,0.2-20,0c-2.9-0.1-3.4,0.9-3.4,3.6
	C61.2,117.1,61.2,131.7,61.2,146.2"/>
</svg>

	</a>
</div>
<span class="copyright"> maísa champalimaud © 2018 </span>
<a href="#" class="arrow-up">
	<svg x="0px" y="0px" viewBox="0 0 225.3 200" style="enable-background:new 0 0 225.3 200;" xml:space="preserve">
<polygon class="black" points="8.3,104.7 25.2,121.6 99.2,47.6 99.7,199 124.8,199 125.2,47.7 199.5,122.3 216.3,105.5 112.1,1 "/>
</svg>

</a>
</div>
</footer>