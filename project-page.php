<?php include 'includes/head.php' ?>
<body>
	<?php include 'includes/nav.php' ?>
	<main id="main">
		<div class="swiper-container swiper-gallery swiper-slideshow">
			<div class="swiper-wrapper">
				<div class="swiper-slide">
					<h2 class="swiper-gallery-title">José Saramago</h2>
					<span class="swiper-gallery-caption">Mixture tecnique: acrylic<br>Measures: 82 x 94 cm<br>2016</span>
					<img src="img/f0235e_37512b32b0e5472ea75e2a64bb9e6db6.jpg">
				</div>
				<div class="swiper-slide">
					<h2 class="swiper-gallery-title">José Saramago</h2>
					<span class="swiper-gallery-caption">Mixture tecnique: acrylic over multiples books<br>Measures: 82 x 94 cm<br>2016</span>
					<img src="img/f0235e_db3d0f83cd7542ed96ce2600d971b8e7.jpg">
				</div>
				<div class="swiper-slide">
					<h2 class="swiper-gallery-title">José Saramago</h2>
					<span class="swiper-gallery-caption">Mixture tecnique: acrylic over multiples books<br>Measures: 82 x 94 cm<br>2016</span>
					<img src="img/f0235e_aff525505e5e4494b0d3f2518bdeea40.jpg">
				</div>
				<div class="swiper-slide">
					<h2 class="swiper-gallery-title">José Saramago</h2>
					<span class="swiper-gallery-caption">Mixture tecnique: acrylic over multiples books<br>Measures: 82 x 94 cm<br>2016</span>
					<img src="img/f0235e_8323089bd5bf4760b82302d4534a162d.jpg">
				</div>
			</div>
			<div class="swiper-pagination visible-xs"></div>
			<div class="swiper-button-prev hidden-xs"><img src="dist/assets/nav-left.svg"></div>
		    <div class="swiper-button-next hidden-xs"><img src="dist/assets/nav-right.svg"></div>
		</div>
		<div class="more-info-toggle-wrapper">
		<div class="more-info-toggle">
			<span></span>
		</div>
		</div>
			<div class="expand-info">
				<div class="container">
					<h2 class="project-title">sobre livros</h2>
					<div class="project-description">
						<p class="basic-paragraph">
							A exposição “Sobre Livros” é uma forma de dar continuidade ao trabalho da artista plástica Maísa Champalimaud com o objetivo de explorar personagens literários sobre múltiplos de livros. Para a artista, em busca de exaltar os intérpretes máximos da língua portuguesa, surgiu a ideia de retratá-los sobre o suporte que no fundo é deles - os livros. Desta forma, em 2014 a pintora lançou a primeira série intitulada “Em Lusofonia” retratando reconhecidos escritores oriundos de países lusófonos.  Em 2016, ao ser convidada para decorar as paredes de apartamentos de um edifício histórico voltado para o alojamento local, lançou a coleção “Ex-Libris”.
						</p>
						<p class="basic-paragraph">
							Desde Gutenberg, um livro pode ser visto como formato de um múltiplo, ou seja, suas páginas reproduzem o mesmo conteúdo com o tempo. Da mesma forma, a artista Maísa Champalimaud optou por multiplicar a arte, aproximando a literatura da pintura ao criar estas três séries.
						</p>
						<p class="basic-paragraph">
							Maisa Champalimaud 2014
						</p>
					</div>
					<div class="similar-projects">
						<h2 class="section-title"> similar projects </h2>
						<div class="row">
							<div class="col-xs-6 col-sm-4 col-md-4 pad-right">
								<a href="project-page.php" class="project-list-item">
									<div class="img-container">
									<img class="" src="img/f0235e_0e607e42acaa403e8219fa262da904ca.jpg" alt="">
								</div>
									<span class="project-list-title">Title title</span>
									<span class="project-list-sub-title">Subtitle subtitle subtitle</span>
								</a>
							</div>
							<div class="col-xs-6 col-sm-4 col-md-4 pad-left">
								<a href="project-page.php" class="project-list-item">
									<div class="img-container">
									<img class="" src="img/f0235e_0e607e42acaa403e8219fa262da904ca.jpg" alt="">
								</div>
									<span class="project-list-title">Title title title </span>
									<span class="project-list-sub-title">Subtitle subtitle subtitle</span>
								</a>
							</div>
							<div class="col-xs-6 col-sm-4 col-md-4 hidden-xs">
								<a href="project-page.php" class="project-list-item">
									<div class="img-container">
									<img class="" src="img/f0235e_0e607e42acaa403e8219fa262da904ca.jpg" alt="">
								</div>
									<span class="project-list-title">Title title title title title</span>
									<span class="project-list-sub-title">Subtitle subtitle subtitle</span>
								</a>
							</div>
						</div>
						<a class="box-link" href="projects.php"> view more </a>
					</div>

				</div>
			</div>
		</main>
		<!-- <?php include 'includes/footer.php' ?> -->

		<script src="dist/js/bundle.js"></script>
	</body>
	</html>
